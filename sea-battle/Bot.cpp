#include "Bot.h"
#include <random>
#include <time.h>

bool Bot::isCanPutShipCell(Coord coordShip)
{
	if (my_.getCellState(coordShip) == NOTHING
		&& my_.getCellState({ coordShip.x - 1, coordShip.y - 1 }) == NOTHING //to the up and left of the target
		&& my_.getCellState({ coordShip.x, coordShip.y - 1 }) == NOTHING //to the up of the target
		&& my_.getCellState({ coordShip.x + 1, coordShip.y - 1 }) == NOTHING //to the up and right of the target
		&& my_.getCellState({ coordShip.x - 1, coordShip.y }) == NOTHING //to the left of the target
		&& my_.getCellState({ coordShip.x + 1, coordShip.y }) == NOTHING //to the right of the target
		&& my_.getCellState({ coordShip.x - 1, coordShip.y + 1 }) == NOTHING //to the down and left of the target
		&& my_.getCellState({ coordShip.x, coordShip.y + 1 }) == NOTHING //to the down of the target
		&& my_.getCellState({ coordShip.x, coordShip.y + 1 }) == NOTHING) { //to the down and left of the target
		return true;
	}
	return false;
}

void Bot::shipToKilled(Coord coordShip, bool isMyField)
{
	// up
	Coord temp = coordShip; temp.y--;
	if (isMyField) {
		while (temp.y >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.y--;
		}
	}
	else {
		while (temp.y >= 0 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.y--;
		}
	}

	// down
	temp = coordShip; temp.y++;
	if (isMyField) {
		while (temp.y <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.y++;
		}
	}
	else {
		while (temp.y <= 9 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.y++;
		}
	}

	// right
	temp = coordShip; temp.x++;
	if (isMyField) {
		while (temp.x <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.x++;
		}
	}
	else {
		while (temp.x <= 9 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.x++;
		}
	}

	// left
	temp = coordShip; temp.x--;
	if (isMyField) {
		while (temp.x >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.x--;
		}
	}
	else {
		while (temp.x >= 0 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.x--;
		}
	}
}

bool Bot::isShipKilled(Coord coordShip)
{
	// up
	Coord temp = coordShip; temp.y--;
	while (temp.y >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.y--;
	}

	// down
	temp = coordShip; temp.y++;
	while (temp.y <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.y++;
	}

	// right
	temp = coordShip; temp.x++;
	while (temp.x <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.x++;
	}

	// left
	temp = coordShip; temp.x--;
	while (temp.x >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.x--;
	}
	return true;
}

Coord Bot::generationTarget()
{
	srand(time(0));
	short x, y;
	do {
		x = rand() % 10;
		y = rand() % 10;
	} while (this->isAlradyShooting({ x, y }));

	return { x, y };
}

void Bot::generationShipPosition()
{
	srand(time(0));
	short x, y, cell, direction;

	for (int i = 0; i < 10; i++) {
		if (i == 0) { cell = 4; }
		else if (i >= 1 && i <= 2) { cell = 3; }
		else if (i >= 3 && i <= 5) { cell = 2; }
		else if (i >= 6 && i <= 9) { cell = 1; }

		do {
			x = rand() % 10;
			y = rand() % 10;
			direction = rand() % 2;
		} while (this->putShip({ x, y }, (direction == 0 ? Direction::RIGHT : Direction::DOWN), cell));
	}
}

int Bot::putShip(Coord coordShip, Direction direction, int sizeShip)
{
	if (coordShip.x < 0 || coordShip.x > 9 || coordShip.y < 0 || coordShip.y > 9)
		return 1; // oops

	// check can put the ship
	Coord c = coordShip;
	for (int i = 0; i < sizeShip; i++) {
		if (!isCanPutShipCell(c) || c.x < 0 || c.x > 9 || c.y < 0 || c.y > 9) {
			return 1; // oops
		}

		direction == Direction::RIGHT ? c.x++ : c.y++;
		//if (direction == RIGHT) {
		//	c.x++;
		//} 
		//else if (direction == DOWN) {
		//	c.y++;
		//}
	}

	// put ship
	c = coordShip;
	for (int i = 0; i < sizeShip; i++) {
		my_.addShipCell(c);
		direction == Direction::RIGHT ? c.x++ : c.y++;
	}

	return 0;
}

void Bot::attackShip(CellState cellState, Coord coordShip)
{
	if (cellState == SHOT)
		enemy_.changeCellState(SHOT, coordShip);

	if (cellState == KNOCKED)
		enemy_.changeCellState(KNOCKED, coordShip);

	if (cellState == KILLED) {
		enemy_.changeCellState(KILLED, coordShip);
		shipToKilled(coordShip, false);
	}
}

CellState Bot::attackOnMyShip(Coord coordShip)
{
	if (my_.getCellState(coordShip) == NOTHING) {
		my_.changeCellState(SHOT, coordShip);
	}
	else if (my_.getCellState(coordShip) == SHIP) {
		my_.changeCellState(KNOCKED, coordShip);
		if (isShipKilled(coordShip) == true) {
			my_.changeCellState(KILLED, coordShip);
			shipToKilled(coordShip, true);
		}
	}

	return my_.getCellState(coordShip);
}

bool Bot::isAlradyShooting(Coord coordShip)
{
	if (enemy_.getCellState(coordShip) == CellState::SHOT || enemy_.getCellState(coordShip) == CellState::KNOCKED
		|| enemy_.getCellState(coordShip) == CellState::KILLED)
		return true;
	else
		return false;
}

Bot::Bot()
{
	cell_ = 4 * 1 + 3 * 2 + 2 * 3 + 1 * 4;
	this->generationShipPosition();
	//this->putShip({ 0, 0 }, Direction::DOWN, 4);
	//this->putShip({ 2, 3 }, Direction::RIGHT, 3);
	//this->putShip({ 2, 1 }, Direction::RIGHT, 3);
	//this->putShip({ 2, 5 }, Direction::RIGHT, 2);
	//this->putShip({ 5, 5 }, Direction::DOWN, 2);
	//this->putShip({ 5, 8 }, Direction::DOWN, 2);
	//this->putShip({ 0, 9 }, Direction::RIGHT, 1);
	//this->putShip({ 6, 0 }, Direction::RIGHT, 1);
	//this->putShip({ 7, 2 }, Direction::DOWN, 1);
	//this->putShip({ 8, 5 }, Direction::RIGHT, 1);
}

int Bot::getCell() const
{
	return cell_;
}

void Bot::decrCell()
{
	cell_--;
}
