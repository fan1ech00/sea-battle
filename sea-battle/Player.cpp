#include "Player.h"

bool Player::isCanPutShipCell(Coord coordShip)
{
	if (my_.getCellState(coordShip) == NOTHING
		&& my_.getCellState({ coordShip.x - 1, coordShip.y - 1 }) == NOTHING //to the up and left of the target
		&& my_.getCellState({ coordShip.x, coordShip.y - 1 }) == NOTHING //to the up of the target
		&& my_.getCellState({ coordShip.x + 1, coordShip.y - 1 }) == NOTHING //to the up and right of the target
		&& my_.getCellState({ coordShip.x - 1, coordShip.y }) == NOTHING //to the left of the target
		&& my_.getCellState({ coordShip.x + 1, coordShip.y }) == NOTHING //to the right of the target
		&& my_.getCellState({ coordShip.x - 1, coordShip.y + 1 }) == NOTHING //to the down and left of the target
		&& my_.getCellState({ coordShip.x, coordShip.y + 1 }) == NOTHING //to the down of the target
		&& my_.getCellState({ coordShip.x, coordShip.y + 1 }) == NOTHING) { //to the down and left of the target
		return true;
	}
	return false;
}

void Player::shipToKilled(Coord coordShip, bool isMyField)
{
	// up
	Coord temp = coordShip; temp.y--;
	if (isMyField) {
		while (temp.y >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.y--;
		}
	}
	else {
		while (temp.y >= 0 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.y--;
		}
	}

	// down
	temp = coordShip; temp.y++;
	if (isMyField) {
		while (temp.y <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.y++;
		}
	}
	else {
		while (temp.y <= 9 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.y++;
		}
	}

	// right
	temp = coordShip; temp.x++;
	if (isMyField) {
		while (temp.x <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.x++;
		}
	}
	else {
		while (temp.x <= 9 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.x++;
		}
	}

	// left
	temp = coordShip; temp.x--;
	if (isMyField) {
		while (temp.x >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
			my_.changeCellState(KILLED, temp);
			temp.x--;
		}
	}
	else {
		while (temp.x >= 0 && enemy_.getCellState(temp) != NOTHING && enemy_.getCellState(temp) != SHOT) {
			enemy_.changeCellState(KILLED, temp);
			temp.x--;
		}
	}
}

int Player::getCell() const
{
	return cell_;
}

void Player::decrCell()
{
	cell_--;
}

bool Player::isShipKilled(Coord coordShip)
{
	// up
	Coord temp = coordShip; temp.y--;
	while (temp.y >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.y--;
	}

	// down
	temp = coordShip; temp.y++;
	while (temp.y <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.y++;
	}

	// right
	temp = coordShip; temp.x++;
	while (temp.x <= 9 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.x++;
	}

	// left
	temp = coordShip; temp.x--;
	while (temp.x >= 0 && my_.getCellState(temp) != NOTHING && my_.getCellState(temp) != SHOT) {
		if (my_.getCellState(temp) == SHIP)
			return false;
		temp.x--;
	}
	return true;
}

void Player::printMyFiled(short x, short y)
{
	my_.printField(x, y);
}

void Player::printEnemyFiled(short x, short y)
{
	enemy_.printField(x, y);
}

int Player::putShip(Coord coordShip, Direction direction, int sizeShip)
{
	if (coordShip.x < 0 || coordShip.x > 9 || coordShip.y < 0 || coordShip.y > 9)
		return 1; // oops

	// check can put the ship
	Coord c = coordShip;
	for (int i = 0; i < sizeShip; i++) {
		if (!isCanPutShipCell(c) || c.x < 0 || c.x > 9 || c.y < 0 || c.y > 9) {
			return 1; // oops
		}

		direction == Direction::RIGHT ? c.x++ : c.y++;
		//if (direction == RIGHT) {
		//	c.x++;
		//} 
		//else if (direction == DOWN) {
		//	c.y++;
		//}
	}

	// put ship
	c = coordShip;
	for (int i = 0; i < sizeShip; i++) {
		my_.addShipCell(c);
		direction == Direction::RIGHT ? c.x++ : c.y++;
	}

	return 0;
}

void Player::attackShip(CellState cellState, Coord coordShip)
{
	if (cellState == SHOT)
		enemy_.changeCellState(SHOT, coordShip);

	else if (cellState == KNOCKED)
		enemy_.changeCellState(KNOCKED, coordShip);

	else if (cellState == KILLED) {
		enemy_.changeCellState(KILLED, coordShip);
		shipToKilled(coordShip, false);
	}
}

CellState Player::attackOnMyShip(Coord coordShip)
{
	if (my_.getCellState(coordShip) == NOTHING) {
		my_.changeCellState(SHOT, coordShip);
	}
	else if (my_.getCellState(coordShip) == SHIP) {
		my_.changeCellState(KNOCKED, coordShip);
		if (isShipKilled(coordShip) == true) {
			my_.changeCellState(KILLED, coordShip);
			shipToKilled(coordShip, true);
		}
	}

	return my_.getCellState(coordShip);
}

bool Player::isAlradyShooting(Coord coordShip)
{
	if (enemy_.getCellState(coordShip) == CellState::SHOT || enemy_.getCellState(coordShip) == CellState::KNOCKED
		|| enemy_.getCellState(coordShip) == CellState::KILLED)
		return true;
	else
		return false;
}

Player::Player()
{
	cell_ = 4 * 1 + 3 * 2 + 2 * 3 + 1 * 4;
}
