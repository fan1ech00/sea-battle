#pragma once
#include "Player.h"
#include "Bot.h"

#define MY_FIELD_POS_X 2
#define MY_FIELD_POS_Y 1
#define ENEMY_FIELD_POS_X 14
#define ENEMY_FIELD_POS_Y 1

enum class GameStatus {
	PUT_SHIPS,
	PLAYER,
	BOT,
};

class Game
{
	GameStatus gameStatus;
	Player player;
	Bot bot;

	void clearWindow();
	void putShips();
	void printFields();
	Coord inputCoord();
public:
	void play();
	Game();
};

