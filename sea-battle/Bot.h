#pragma once
#include "Field.h"

class Bot
{
	Field my_;
	Field enemy_;
	int cell_;

	bool isCanPutShipCell(Coord coordShip);
	bool isShipKilled(Coord coordShip);
	void shipToKilled(Coord coordShip, bool isMyField);
public:
	int getCell() const;
	void decrCell(); // decrement

	Coord generationTarget();
	void generationShipPosition();

	int putShip(Coord coordShip, Direction direction, int sizeShip);
	void attackShip(CellState cellState, Coord coordShip);
	CellState attackOnMyShip(Coord coordShip);
	bool isAlradyShooting(Coord coordShip);

	Bot();
};

