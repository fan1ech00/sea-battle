#include <iostream>
#include <Windows.h>
#include "Game.h"

#define WINDOW_TITLE "SEA BATTLE"
#define WINDOW_COLS 40
#define WINDOW_LINES 17
#define WINDOW_FONT_SIZE 36
#define WINDOW_FONT_NAME L"Consolas"

void changeFont(HANDLE hOut, int fontSize, const wchar_t* fontName);
void resizeWindow(int cols, int lines);
void cursorVisible(HANDLE hOut, bool isVisible);

int main()
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	cursorVisible(hOut, false);
	SetConsoleTitleA(WINDOW_TITLE);
	resizeWindow(WINDOW_COLS, WINDOW_LINES);
	changeFont(hOut, WINDOW_FONT_SIZE, WINDOW_FONT_NAME);

	while (true) {
		Game game;
		game.play();
	}
}

void changeFont(HANDLE hOut, int fontSize, const wchar_t* fontName)
{
	CONSOLE_FONT_INFOEX cfi;
	cfi.cbSize = sizeof(cfi);
	cfi.dwFontSize.Y = fontSize;    // Height
	wcscpy_s(cfi.FaceName, fontName); // Choose your font
	SetCurrentConsoleFontEx(hOut, FALSE, &cfi);
}

void resizeWindow(int cols, int lines)
{
	char str[30];
	wsprintfA(str, "mode con cols=%d lines=%d", cols, lines);
	system(str);
}

void cursorVisible(HANDLE hOut, bool isVisible)
{
	CONSOLE_CURSOR_INFO cur;
	cur.bVisible = isVisible;
	cur.dwSize = 100;
	SetConsoleCursorInfo(hOut, &cur);
}