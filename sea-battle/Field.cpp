#include "Field.h"
#include <iostream>
#include <Windows.h>

void Field::printField(short cX, short cY)
{
	HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD coord_temp = { cX, cY };

	// print coords
	SetConsoleCursorPosition(hOut, coord_temp);
	std::cout << " ABCDEFGHIJ" << std::endl;
	for (int i = 0; i <= 9; i++) {
		coord_temp.Y++; SetConsoleCursorPosition(hOut, coord_temp);
		std::cout << i;
	}

	// print filed
	for (short y = 0; y < 10; y++) {
		for (short x = 0; x < 10; x++) {
			if (grid[y][x].state == SHIP) {
				SetConsoleCursorPosition(hOut, { cX + 1 + x, cY + 1 + y });
				std::cout << (char)177;
			} 
			else if (grid[y][x].state == KNOCKED) {
				SetConsoleCursorPosition(hOut, { cX + 1 + x, cY + 1 + y });
				SetConsoleTextAttribute(hOut, FOREGROUND_RED);
				std::cout << (char)177;
				SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (grid[y][x].state == KILLED) {
				SetConsoleCursorPosition(hOut, { cX + 1 + x, cY + 1 + y });
				SetConsoleTextAttribute(hOut, FOREGROUND_RED);
				std::cout << 'X';
				SetConsoleTextAttribute(hOut, FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
			}
			else if (grid[y][x].state == SHOT) {
				SetConsoleCursorPosition(hOut, { cX + 1 + x, cY + 1 + y });
				std::cout << (char)197;
			}
			else if (grid[y][x].state == NOTHING) {
				SetConsoleCursorPosition(hOut, { cX + 1 + x, cY + 1 + y });
				std::cout << " ";
			}
		}
	}
}

void Field::addShipCell(Coord coord)
{
	changeCellState(SHIP, coord);
}

void Field::addShotCell(Coord coord)
{
	changeCellState(SHOT, coord);
}

void Field::changeCellState(CellState state, Coord coord)
{
	if (coord.x >= 0 && coord.x < 10 && coord.y >= 0 && coord.y < 10)
		grid[coord.y][coord.x].state = state;
	else
		return;
}

CellState Field::getCellState(Coord coord)
{
	if (coord.x >= 0 && coord.x < 10 && coord.y >= 0 && coord.y < 10) {
		return grid[coord.y][coord.x].state;
	}
	return NOTHING;
}

Field::Field()
{
	for (int i = 0; i < 10; i++) {
		for (int y = 0; y < 10; y++) {
			grid[i][y].state = NOTHING;
		}
	}
}

