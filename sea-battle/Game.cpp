#include "Game.h"
#include <iostream>
#include <Windows.h>

void Game::clearWindow()
{
	system("cls");
}

void Game::putShips()
{
	player.putShip({ 0, 0 }, Direction::DOWN, 4);
	player.putShip({ 2, 3 }, Direction::RIGHT, 3);
	player.putShip({ 2, 1 }, Direction::RIGHT, 3);
	player.putShip({ 2, 5 }, Direction::RIGHT, 2);
	player.putShip({ 5, 5 }, Direction::DOWN, 2);
	player.putShip({ 5, 8 }, Direction::DOWN, 2);
	player.putShip({ 0, 9 }, Direction::RIGHT, 1);
	player.putShip({ 6, 0 }, Direction::RIGHT, 1);
	player.putShip({ 7, 2 }, Direction::DOWN, 1);
	player.putShip({ 8, 5 }, Direction::RIGHT, 1);

	///////// WORKED
	//char coordX;
	//short _coordX;
	//short coordY;
	//char shipDirection;
	//int cell;

	//for (int i = 0; i < 10; i++) {
	//	do {
	//		clearWindow();

	//		if (i == 0) { cell = 4; std::cout << "ship of 4 cell"; }
	//		else if (i >= 1 && i <= 2) { cell = 3; std::cout << "ship of 3 cell"; }
	//		else if (i >= 3 && i <= 5) { cell = 2; std::cout << "ship of 2 cell"; }
	//		else if (i >= 6 && i <= 9) { cell = 1; std::cout << "ship of 1 cell"; }

	//		player.printMyFiled(MY_FIELD_POS_X, MY_FIELD_POS_Y);
	//		std::cout << std::endl << std::endl;
	//		std::cout << "Enter in format [A-J][0-9][RIGHT/DOWN], ex: D7R, G0D:" << std::endl;
	//		std::cin.clear();
	//		std::cin >> coordX >> coordY >> shipDirection;

	//		_coordX = coordX - 65;


	//	} while (coordX < 'A' || coordX > 'J' || coordY < 0 || coordY >= 10 
	//		|| (shipDirection != 'R' && shipDirection != 'D')
	//		|| player.putShip({ _coordX, coordY }, (shipDirection == 'R' ? Direction::RIGHT : Direction::DOWN), cell));
	//}
}

void Game::printFields()
{
	player.printMyFiled(MY_FIELD_POS_X, MY_FIELD_POS_Y);
	player.printEnemyFiled(ENEMY_FIELD_POS_X, ENEMY_FIELD_POS_Y);
	std::cout << std::endl << std::endl;
}

Coord Game::inputCoord()
{
	char coordX;
	short coordY;
	do {
		clearWindow();
		printFields();
		std::cout << "Enter in format [A-J][0-9], ex: D7, G0:" << std::endl;
		std::cin.clear();
		std::cin >> coordX >> coordY;
	} while (coordX < 'A' || coordX > 'J' || coordY < 0 || coordY >= 10 
		|| player.isAlradyShooting({ (short)coordX - (short)65, coordY }));

	return { (short)coordX - 65, coordY };
}

void Game::play()
{
	clearWindow();

	while (true) {	
		if (player.getCell() == 0 || bot.getCell() == 0) {
			printFields();
			Sleep(4000);
			break;
		}
			
		clearWindow();
		printFields();
		if (gameStatus == GameStatus::PUT_SHIPS) {
			putShips();
			gameStatus = GameStatus::PLAYER;
		}
		else if (gameStatus == GameStatus::PLAYER) {
			Coord target = inputCoord();
			CellState receivedState = bot.attackOnMyShip(target);
			player.attackShip(receivedState, target);

			if (receivedState != CellState::KNOCKED && receivedState != CellState::KILLED)
				gameStatus = GameStatus::BOT;
			else
				bot.decrCell();
		}
		else if (gameStatus == GameStatus::BOT) {
			Sleep(1000);
			Coord target = bot.generationTarget();
			CellState receivedState = player.attackOnMyShip(target);
			bot.attackShip(receivedState, target);

			if (receivedState != CellState::KNOCKED && receivedState != CellState::KILLED)
				gameStatus = GameStatus::PLAYER;
			else
				player.decrCell();
		}
	}

}

Game::Game()
{
	gameStatus = GameStatus::PUT_SHIPS;
}
