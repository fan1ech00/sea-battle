#pragma once

enum class Direction {
	RIGHT,
	DOWN
};

struct Coord {
	short x;
	short y;
};

enum CellState {
	NOTHING, // empty
	KNOCKED, // injured
	KILLED,
	SHOT,
	SHIP,
};

struct Cell {
	CellState state;
};

class Field
{
	Cell grid[10][10];
public:
	void printField(short cX, short cY);
	void addShipCell(Coord coord);
	void addShotCell(Coord coord);
	void changeCellState(CellState state, Coord coord);
	CellState getCellState(Coord coord);

	Field();
};

